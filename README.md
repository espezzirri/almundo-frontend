* **Se utilizó el sistema de grillas de bootstrap.**
* **Se utilizaron 2 directivas externas: slider de fotos y el rango de precios.**
* **Se desarrollaron los filtros de nombre de hotel, rango de precios y cantidad de estrellas.**
* **Se desarrollo el ordenamiento por relevancia, precio y estrellas.**
* **Se utilizó el preprocesador de css sass**
* **Se utilizaron iconos de "font-awesome"**


Al ingresar a la pagina de hoteles genera un listado de hoteles con valores aleatorios con el siguiente formato:

```
#!json

[
   {
      "name":"Hotel Emperador",
      "stars":1,
      "price":6703,
      "relevant":5,
      "services":[
         {
            "name":"Aire acondicionado",
            "icon":"snowflake-o"
         },
         {
            "name":"Bar",
            "icon":"glass"
         },
         {
            "name":"WiFi",
            "icon":"wifi"
         },
         {
            "name":"Mascotas",
            "icon":"paw"
         },
         {
            "name":"Desayuno",
            "icon":"coffee"
         }
      ],
      "images":[
         11,
         6,
         10
      ]
   },
   {
     ...
   },
   ...
]
```