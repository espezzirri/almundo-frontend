(function() {
    "use strict";

    angular
        .module('almundo', [
            'ngRoute',
            'angularRangeSlider',
            'slick'
        ]);

})();
