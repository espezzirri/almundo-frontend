(function() {

  angular
    .module("almundo")
    .controller("HotelesController", HotelesController);

    HotelesController.$inject = ['hotelsService'];
  
    function HotelesController(hotelsService) {
  
      var vm = this;
  
      init();
      getHotels();
  
  
      function init() {
        
        vm.hotels = [];
        
        vm.filter = {
          name: '',
          stars: [],
          priceFrom: 0,
          priceTo: 100
        };
  
  
        vm.sort = {
          field: 'relevant',
          order: false
        };
        
        vm.page = {
          size: 5,
          current: 0
        };
        
        vm.rangePrice = {
          min: 0,
          max: 100
        };
  
        vm.starsCount = {
          "1": 0,
          "2": 0,
          "3": 0,
          "4": 0,
          "5": 0
        };
  
        vm.sortOptions = [{
          name: 'Más relevantes',
          field: 'relevant',
          order: false
        }, {
          name: 'Menor precio',
          field: 'price',
          order: false
        }, {
          name: 'Mayor precio',
          field: 'price',
          order: true
        }, {
          name: 'Más estrellas',
          field: 'stars',
          order: true
        }, {
          name: 'Menos estrellas',
          field: 'stars',
          order: false
        }];
  
      }
  
  
      function getHotels() {
        hotelsService.getHotels().then(
          getHotelsSuccess,
          getHotelsfail
        );
      }
  
  
      function getHotelsSuccess(hotels) {
        vm.hotels = hotels;
  
        vm.filter.priceFrom = vm.rangePrice.min = Math.min.apply(Math, vm.hotels.map(function(hotel) {
          return hotel.price;
        }));
  
        vm.filter.priceTo = vm.rangePrice.max = Math.max.apply(Math, vm.hotels.map(function(hotel) {
          return hotel.price;
        }));
  
        countStars(hotels);
  
      };
      
      
      function getHotelsfail() {
        console.log("se produjo un error al cargar los hoteles")
      };

      function countStars(hoteles){
        angular.forEach(hoteles, function(hotel){
          vm.starsCount[hotel.stars]++;  
        });
      }

  };

})();
