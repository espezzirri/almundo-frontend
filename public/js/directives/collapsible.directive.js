(function() {
    "use strict";

    angular
        .module('almundo')
        .directive('amCollapsible', Collapsible);

    function Collapsible() {

        var directive = {
            restrict: 'E',
            transclude: true,
            scope: {
                title: '@',
                icon: '@'
            },
            templateUrl: 'public/templates/directives/collapsible.html'
        };

        return directive;
    };


})();
