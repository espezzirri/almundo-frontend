(function() {
    "use strict";

    angular
        .module('almundo')
        .directive('amHotelFrame', HotelFrame);


    function HotelFrame() {

        var directive = {
            restrict: 'E',
            scope: {
                hotel: '='
            },
            controller: HotelFrameController,
            templateUrl: 'public/templates/directives/hotel_frame.html'
        };

        return directive;
    };


    HotelFrameController.$inject = ['$scope'];


    function HotelFrameController($scope) {
        $scope.getTimes = function(n) {
            return new Array(parseInt(n));
        };
    }


})();
