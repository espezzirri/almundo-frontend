(function() {
    "use strict";
    
    angular
        .module('almundo')
        .directive('amPagination', Pagination);


        function Pagination() {
    
            var directive = {
                restrict: 'E',
                scope: {
                    totalItems: '=',
                    pageSize: '=',
                    currentPage: '='
                },
                controller: PaginationController,
                templateUrl: 'public/templates/directives/pagination.html'
            };
    
            return directive;
        };
    
    
        PaginationController.$inject = ['$scope'];
    
    
        function PaginationController($scope) {
    
            var maxButtons = 5;
    
            $scope.currentPage = 0;
            $scope.buttons = [];
            $scope.pageBlocksNumber = 0;
            $scope.currentPageBlock = 0;
    
    
            $scope.min = Math.min;
            $scope.getLastPage = getLastPage;
            $scope.changePage = changePage;
    
    
            $scope.$watch('[totalItems, pageSize, currentPage]', function() {
                updateButtons();
            });
    
    
            function getLastPage() {
                var last = (Math.ceil($scope.totalItems / $scope.pageSize)).toFixed(0) - 1;
                return last < 0 ? 0 : last;
            }
    
    
            function changePage(page) {
                if (page >= 0 && page <= $scope.getLastPage()) {
                    $scope.currentPage = page;
                }
                updateButtons();
            };
    
    
            function updateButtons() {
                $scope.buttons = [];
    
                if ($scope.currentPage > $scope.getLastPage()) {
                    $scope.currentPage = $scope.getLastPage();
                }
    
                if ($scope.getLastPage() < maxButtons) {
                    $scope.pageBlocksNumber = 1;
                    $scope.currentPageBlock = 0;
                }
                else {
                    $scope.pageBlocksNumber = Math.ceil($scope.getLastPage() / maxButtons).toFixed(0);
                    $scope.currentPageBlock = Math.floor($scope.currentPage / maxButtons);
                }
    
                $scope.minIndex = ($scope.currentPageBlock * maxButtons);
                $scope.maxIndex = $scope.minIndex + maxButtons - 1;
    
                if ($scope.maxIndex > $scope.getLastPage())
                    $scope.maxIndex = $scope.getLastPage();
    
                if (!($scope.minIndex == 0 && $scope.maxIndex == 0))
                    for (var i = $scope.minIndex; i <= $scope.maxIndex; i++) {
                        $scope.buttons.push(i);
                    }
            };
        }


})();
