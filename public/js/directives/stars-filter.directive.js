(function() {
    "use strict";

    angular
        .module('almundo')
        .directive('amStarsFilter', StarsFilter);


    function StarsFilter() {

        var directive = {
            restrict: 'E',
            scope: {
                stars: '=',
                starsCount: '='
            },
            controller: StarsFilterController,
            templateUrl: 'public/templates/directives/stars_filter.html'
        };

        return directive;
    };

    StarsFilterController.$inject = ['$scope'];

    function StarsFilterController($scope) {

        $scope.noFilter = noFilter;
        $scope.setNoFilter = setNoFilter;
        $scope.exists = exists;
        $scope.toggle = toggle;


        function noFilter() {
            return $scope.stars.length == 0;
        }


        function setNoFilter() {
            $scope.stars = [];
            $scope.allStars = true;
        }

        function exists(number) {
            return $scope.stars.indexOf(number) > -1;
        }

        function toggle(number) {
            var index;
            if ((index = $scope.stars.indexOf(number)) == -1) {
                $scope.stars.push(number);
            }
            else {
                $scope.stars.splice(index, 1);
            }
        }
    }

})();
