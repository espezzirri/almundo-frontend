(function() {

    angular
        .module('almundo')
        .filter('hotelesFilter', hotelesFilter);

    function hotelesFilter() {
        return function(hotels, filters) {

            var filtered = [];

            for (var i = 0; i < hotels.length; i++) {
                var hotel = hotels[i];

                if (hotel.price >= filters.priceFrom && hotel.price <= filters.priceTo &&
                    (filters.stars.length == 0 || filters.stars.indexOf(hotel.stars) > -1) &&
                    (filters.name == '' || angular.isUndefined(filters.name) || hotel.name.toLowerCase().indexOf(filters.name.toLowerCase()) > -1)) {

                    filtered.push(hotel);
                }
            }
            return filtered;
        };
    };

})();
