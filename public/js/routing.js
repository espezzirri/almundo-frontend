(function() {
    "use strict";

    angular
        .module('almundo')
        .config(Config);

    Config.$inject = ['$routeProvider'];

    function Config($routeProvider) {
        $routeProvider.
        when('/hoteles', {
            templateUrl: 'public/templates/hoteles.html',
            controller: 'HotelesController',
            controllerAs: 'hoteles'
        }).
        when('/vuelos', {
            templateUrl: 'public/templates/vuelos.html'
        }).
        when('/vuelos-hotel', {
            templateUrl: 'public/templates/vuelos_hotel.html'
        }).
        otherwise({
            redirectTo: '/hoteles'
        });
    }


})();
