(function() {
    
    angular
        .module('almundo')
        .factory('hotelsService', hotelsService);
    
    
    hotelsService.$inject = ['$q', 'hotelsGeneratorService'];
    
    
    function hotelsService($q, hotelsGeneratorService) {
        
        var service = {
            getHotels: getHotels
        };
        
        return service;
    

        function getHotels(){
            
            var deferred = $q.defer();

              setTimeout(function() {
                
                var hotels = hotelsGeneratorService.generate(); 
                
                deferred.resolve(hotels);
                
              }, 50);
            
              return deferred.promise;
            
        }

    };

})();