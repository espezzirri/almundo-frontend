(function() {
    
    angular
        .module('almundo')
        .factory('hotelsGeneratorService', hotelsGeneratorService);

    
    function hotelsGeneratorService() {
        
        var service = {
            generate: generate
        };
        
        return service;

        
        function generate() {
            
            var hotels = [];
            
            var names = [   "Hotel Emperador",
                            "Petit Palace San Bernardo",
                            "Hotel Nuevo Boston",
                            "Arenales Suites",
                            "Apartamento Lopez",
                            "Zenit Abeba",
                            "Slow Suites Luchana",
                            "Tryp Madrid Alameda",
                            "Prado Apartamentos",
                            "Clement Barajas",
                            "Hotel Osuna",
                            "Axor Barajas",
                            "Gran Via Capital",
                            "Petit Palace Madrid",
                            "Hostal JQ Madrid",
                            "Hotel Serrano",
                            "Hotel Paseo del Arte",
                            "Apartment La Latina"
                        ];
            
            angular.forEach(names, function(name){
                var hotel = randomHotelGenerator(name);
                hotels.push(hotel);
            });
            
            return hotels;
            
        }
        
            
        function randomHotelGenerator(name){
            
            var hotel = {
                    name: name,
                    stars: between(1, 5),
                    price: between(800, 10000),
                    relevant: between(1, 10),
                    services: randomServices(),
                    images: randomImageGenerator(),
                    disccount: randomDisccount()
                }
                
            return hotel;
        }
        
        function randomServices(){
            
            var services = [
    			  { "name": "Aire acondicionado", "icon": "snowflake-o" },
    			  { "name": "Bar", "icon": "glass" },
    			  { "name": "WiFi", "icon": "wifi" },
    			  { "name": "Mascotas", "icon": "paw" },
    			  { "name": "Desayuno", "icon": "coffee" }
    			 ];
    	    
    	    var randServices = services.filter(function(){
    	        return Math.random() <= 0.6;
    	    });
    			 
    	    return randServices;
            
        }
        
        function randomImageGenerator(){
            var images = [];
            var i = 0;
            while(i < 3){
                var imageId = between(1, 15);
                if(images.indexOf(imageId) == -1){
                    images.push(imageId);
                    i++;
                }
            }
            return images;
        }
        
        function randomDisccount(){
            var disc = between(0, 100);
            if(disc > 10 && disc < 35)
                return disc;
        }
        
        function between(first, last){
            return Math.floor(Math.random() * last) + first;
        }
        
    };

})();